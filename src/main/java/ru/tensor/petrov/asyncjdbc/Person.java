package ru.tensor.petrov.asyncjdbc;

import ru.tensor.petrov.asyncjdbc.db.types.IDbField;
import ru.tensor.petrov.asyncjdbc.db.types.common.Number;
import ru.tensor.petrov.asyncjdbc.db.types.common.VarChar;

import static ru.tensor.petrov.asyncjdbc.common.Util.asArray;

public class Person {

    public static final Number
            id = new Number("id");

    public static final VarChar
            firstName = new VarChar("firstName", 80),
            lastName  = new VarChar("lastName", 80),
            email     = new VarChar("email", 150);

    public static IDbField[] allFields() {
        return asArray(
                id, firstName, lastName, email
        );
    }
}