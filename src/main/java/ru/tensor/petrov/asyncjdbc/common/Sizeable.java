package ru.tensor.petrov.asyncjdbc.common;

public interface Sizeable {
    int size();
}