package ru.tensor.petrov.asyncjdbc.db;

import ru.tensor.petrov.asyncjdbc.db.types.FieldMapping;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.Condition;
import ru.tensor.petrov.util.cortages.PairArr;

public class BuilderUtil {

    public static PairArr<Condition, FieldMapping> mappings(
            Condition condition, FieldMapping... mappings
    ) {
        return new PairArr<>(
                condition, mappings
        );
    }
}