package ru.tensor.petrov.asyncjdbc.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import ru.tensor.petrov.asyncjdbc.db.pm.impl.PmSelectOne;
import ru.tensor.petrov.asyncjdbc.db.types.FieldMapping;
import ru.tensor.petrov.asyncjdbc.db.types.TableSpec;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.Condition;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorNoIndex;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;
import ru.tensor.petrov.asyncjdbc.util.fi.SupplierSqle;
import ru.tensor.petrov.util.cortages.PairArr;
import ru.tensor.petrov.util.fi.Procedure;

import static java.sql.DriverManager.getConnection;
import static ru.tensor.petrov.asyncjdbc.util.errordump.ErrorDump.dumpErrorToFile;
import static ru.tensor.petrov.util.arrays.ArrayCopying.copyToArray;
import static ru.tensor.petrov.util.arrays.ArrayCopying.mapTo;
import static ru.tensor.petrov.util.strings.StringJoining.join_viaComma;

public class DbProvider {

    private static final int DEFAULT_VALIDATION_TIMEOUT = 3;

    //<editor-fold defaultstate="collapsed" desc="set connectionUri, login, password">
    private final String connectionUri, login, password;
    private final int validationTimeout;

    public DbProvider(String connectionUri, String login, String password, int validationTimeout) {
        this.connectionUri = connectionUri;
        this.login = login;
        this.password = password;
        this.validationTimeout = validationTimeout;
        applyDefaultConnectionFactory();
    }

    public DbProvider(String connectionUri, String login, String password) {
        this(connectionUri, login, password, DEFAULT_VALIDATION_TIMEOUT);
    }
    //</editor-fold>

    private void applyDefaultConnectionFactory() {
        connectionFactory = defaultConnectionFactory;
    }

    private SupplierSqle<Connection> openingConnectionFactory() {
        return () -> {
            Connection connection = getConnection(connectionUri, login, password);
            connectionFactory = () -> connection;
            return connection;
        };
    }

    private SupplierSqle<Connection> defaultConnectionFactory = openingConnectionFactory();
    private SupplierSqle<Connection> connectionFactory = defaultConnectionFactory;

    public synchronized Connection getValidOrNewConnection() throws SQLException {
        SupplierSqle<Connection> cf = connectionFactory;
        if (cf != openingConnectionFactory()) {
            Connection cachedConnection = connectionFactory.get();
            if (isValid(cachedConnection)) {
                return cachedConnection;
            } else {
                try {
                    cachedConnection.close();
                } catch (SQLException sqle) {
                    dumpErrorToFile("DbProvider close notValidConnection ", "Ошибка при закрытии не валидного соединения: " + sqle);
                }
                applyDefaultConnectionFactory();
            }
        }
        return connectionFactory.get();
    }

    private boolean isValid(Connection connection) {
        try {
            return connection.isValid(validationTimeout);
        } catch (SQLException ex) {
            return false;
        }
    }


    //<editor-fold defaultstate="collapsed" desc="select">
    public PreparedMappingBuilder select(
            FieldMapping... fields
    ) {
        return new PreparedMappingBuilder();
    }

    public <T extends TableSpec> PreparedMapping selectOneRow(
            Supplier<T> tableSpecProvider,
            Function<T, PairArr<Condition, FieldMapping>> operations
    ) {
        return selectOneRow(tableSpecProvider, operations, () -> {});
    }

    public <T extends TableSpec> PreparedMapping selectOneRow(
            Supplier<T> tableSpecProvider,
            Function<T, PairArr<Condition, FieldMapping>> operations,
            Procedure onEmptyResult
    ) {
        T tableSpec = tableSpecProvider.get();

        PairArr<Condition, FieldMapping> pair = operations.apply(tableSpec);
        Condition where = pair.a;
        FieldMapping[] mappings = pair.b;

        StringBuilder sql = new StringBuilder(200);
        sql.append("SELECT ");
        join_viaComma(sql, mappings, e -> e.fieldName);

        sql.append(" FROM ").append(tableSpec.name());

        List<PsInjectorNoIndex> listInjectors = new ArrayList<>();

        sql.append(" WHERE ");
        where.unrollTo(sql, listInjectors);

        RsExtractor[] extractors = mapTo(mappings, RsExtractor[]::new, e -> e.action);
        PsInjectorNoIndex[] injectors = copyToArray(listInjectors, PsInjectorNoIndex[]::new);

        return new PmSelectOne(
                extractors, injectors, this, sql.toString(), onEmptyResult
        );
    }
    //</editor-fold>


//    public boolean hasOpenedConnection() {
//        return connectionFactory != defaultConnectionFactory();
//    }

    public Statement createStatement() throws SQLException {
        return getValidOrNewConnection().createStatement();
    }

    public PreparedStatement prepareStatement(String sql) throws SQLException {
        return getValidOrNewConnection().prepareStatement(sql);
    }

    public synchronized void shutdown() throws SQLException {
        SupplierSqle<Connection> closedSupplier = () -> {
            throw new SQLException("DbProvider has been shutdowned");
        };
        
        SupplierSqle<Connection> cf = connectionFactory;

        connectionFactory        = closedSupplier;
        defaultConnectionFactory = closedSupplier;

        if (cf != openingConnectionFactory() && cf != closedSupplier) {
            cf.get().close();
        }
    }
}