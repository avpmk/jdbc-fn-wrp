package ru.tensor.petrov.asyncjdbc.db;

import java.util.function.Consumer;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.Condition;
import ru.tensor.petrov.asyncjdbc.db.types.ordering.Orderable;

public class MultipleRowPreparedMappingBuilder extends PreparedMappingBuilder {

    public MultipleRowPreparedMappingBuilder where(Condition... conditions) {
        return this;
    }

    public MultipleRowPreparedMappingBuilder orderBy(Orderable... orderables) {
        return this;
    }

    //<editor-fold defaultstate="collapsed" desc="on events">
    public MultipleRowPreparedMappingBuilder onBeforeSelection(Consumer<String> action) {
        return this;
    }

    public MultipleRowPreparedMappingBuilder onBeforeRow(Consumer<String> action) {
        return this;
    }

    public MultipleRowPreparedMappingBuilder onAfterRow(Consumer<String> action) {
        return this;
    }

    public MultipleRowPreparedMappingBuilder onAfterSelection(Consumer<String> action) {
        return this;
    }
    //</editor-fold>

}