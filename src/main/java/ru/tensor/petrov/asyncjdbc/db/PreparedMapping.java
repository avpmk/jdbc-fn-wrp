package ru.tensor.petrov.asyncjdbc.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import ru.tensor.petrov.asyncjdbc.util.fi.SupplierSqle;

public abstract class PreparedMapping {

    private final DbProvider dbProvider;
    private final String sql;

    public PreparedMapping(DbProvider dbProvider, String sql) {
        this.dbProvider = dbProvider;
        this.sql = sql;
        applyDefaults();
    }

    public abstract void apply() throws SQLException;

    protected SupplierSqle<PreparedStatement> preparedStatement;

    protected final void applyDefaults() {
        preparedStatement = defualtPs();
    }

    private SupplierSqle<PreparedStatement> defualtPs() {
        return () -> {
            PreparedStatement ps = dbProvider.prepareStatement(sql);
            preparedStatement = () -> ps;
            return ps;
        };
    }
}