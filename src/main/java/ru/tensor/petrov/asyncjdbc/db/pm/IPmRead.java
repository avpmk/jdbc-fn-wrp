package ru.tensor.petrov.asyncjdbc.db.pm;

import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;

public interface IPmRead {
    RsExtractor[] extractors();
//    default 
}