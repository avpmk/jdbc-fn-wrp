package ru.tensor.petrov.asyncjdbc.db.pm;

import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorNoIndex;

public interface IPmWrite {
    PsInjectorNoIndex[] injectors();
}