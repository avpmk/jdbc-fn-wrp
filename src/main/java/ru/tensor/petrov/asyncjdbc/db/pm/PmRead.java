package ru.tensor.petrov.asyncjdbc.db.pm;

import ru.tensor.petrov.asyncjdbc.db.DbProvider;
import ru.tensor.petrov.asyncjdbc.db.PreparedMapping;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;

public abstract class PmRead extends PreparedMapping implements IPmRead {

    protected final RsExtractor[] extractors;

    public PmRead(RsExtractor[] extractors, DbProvider dbProvider, String sqlText) {
        super(dbProvider, sqlText);
        this.extractors = extractors;
    }

    @Override public RsExtractor[] extractors() {
        return extractors;
    }
}