package ru.tensor.petrov.asyncjdbc.db.pm;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import ru.tensor.petrov.asyncjdbc.db.DbProvider;
import ru.tensor.petrov.asyncjdbc.db.PreparedMapping;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorNoIndex;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;

public abstract class PmReadWrite extends PreparedMapping {

    protected final RsExtractor[] extractors;
    protected final PsInjectorNoIndex[] injectors;

    protected void injectAll(PreparedStatement ps) throws SQLException {
        for (PsInjectorNoIndex injector : injectors) {
            injector.inject(ps);
        }
    }

    public PmReadWrite(RsExtractor[] extractors,
            PsInjectorNoIndex[] injectors,
            DbProvider dbProvider,
            String sqlText
    ) {
        super(dbProvider, sqlText);
        this.extractors = extractors;
        this.injectors = injectors;
    }

//    @Override public void apply() {
//    }
}