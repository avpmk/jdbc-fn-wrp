package ru.tensor.petrov.asyncjdbc.db.pm.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import ru.tensor.petrov.asyncjdbc.db.DbProvider;
import ru.tensor.petrov.asyncjdbc.db.pm.PmReadWrite;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorNoIndex;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;
import ru.tensor.petrov.util.fi.Procedure;

public class PmSelectOne extends PmReadWrite {

    private final Procedure onEmptyResult;

    public PmSelectOne(RsExtractor[] extractors, PsInjectorNoIndex[] injectors, DbProvider dbProvider, String sqlText, Procedure onEmptyResult) {
        super(extractors, injectors, dbProvider, sqlText);
        this.onEmptyResult = onEmptyResult;
    }

    @Override public void apply() throws SQLException {
        try (ResultSet rs = openRs()) {
            if (rs.next()) {
                int index = 0;
                for (RsExtractor extractor : extractors) {
                    extractor.extract(++index, rs);
                }
            } else {
                onEmptyResult.apply();
            }
        }
    }

    //todo перетаащить в super класс
    protected ResultSet openRs() throws SQLException {
        PreparedStatement ps = preparedStatement.get();
        try {
            injectAll(ps);
            return ps.executeQuery();
        } catch (SQLException sqle) {
            applyDefaults();
            ps = preparedStatement.get();
            injectAll(ps);
            return ps.executeQuery();
        }
    }
}