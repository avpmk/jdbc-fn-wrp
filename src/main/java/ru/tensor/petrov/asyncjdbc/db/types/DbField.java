package ru.tensor.petrov.asyncjdbc.db.types;

import ru.tensor.petrov.asyncjdbc.db.types.ordering.Orderable;

public abstract class DbField implements IDbField, Orderable {

    public final String name;

    public DbField(String name) {
        this.name = name;
    }

    @Override public String name() {
        return name;
    }

    public abstract String type();
}