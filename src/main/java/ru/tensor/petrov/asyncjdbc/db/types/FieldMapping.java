package ru.tensor.petrov.asyncjdbc.db.types;

import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;

public class FieldMapping<A extends RsExtractor> {

    public final String fieldName;
    public final A action;

    public FieldMapping(DbField field, A action) {
        this.fieldName = field.name;
        this.action = action;
    }

    public FieldMapping(IDbField field, A action) {
        this.fieldName = field.name();
        this.action = action;
    }
}