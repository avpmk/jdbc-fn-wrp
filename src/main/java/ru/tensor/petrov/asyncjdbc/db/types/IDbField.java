package ru.tensor.petrov.asyncjdbc.db.types;

public interface IDbField {
    String name();
}