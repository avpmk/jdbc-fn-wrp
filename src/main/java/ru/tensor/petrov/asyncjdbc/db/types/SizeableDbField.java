package ru.tensor.petrov.asyncjdbc.db.types;

import ru.tensor.petrov.asyncjdbc.common.Sizeable;

public abstract class SizeableDbField extends DbField implements Sizeable {

    public final int size;

    public SizeableDbField(String name, int size) {
        super(name);
        this.size = size;
    }

    @Override public int size() {
        return size;
    }
}