package ru.tensor.petrov.asyncjdbc.db.types;

public interface TableSpec {

    String name();
    IDbField[] allFields();
}