package ru.tensor.petrov.asyncjdbc.db.types.common;

import ru.tensor.petrov.asyncjdbc.db.types.DbField;
import ru.tensor.petrov.asyncjdbc.db.types.FieldMapping;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.get.StreamExtractor;

public class Blob extends DbField {

    public Blob(String name) {
        super(name);
    }

    //<editor-fold defaultstate="collapsed" desc="mappings (short names)">
    public FieldMapping to(StreamExtractor extractor) {
        return new FieldMapping(this, extractor);
    }
    //</editor-fold>

    @Override public String type() {
        return "Blob";
    }
}