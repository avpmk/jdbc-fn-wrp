package ru.tensor.petrov.asyncjdbc.db.types.common;

import java.time.LocalDate;
import java.time.Period;
import java.util.function.Consumer;
import java.util.function.Supplier;
import ru.tensor.petrov.asyncjdbc.db.types.DbField;
import ru.tensor.petrov.asyncjdbc.db.types.FieldMapping;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.get.DateExtractor;

public class Date extends DbField {

    public Date(String name) {
        super(name);
    }

    public FieldMapping asDateTo(
            DateExtractor fieldSetter
    ) {
        return new FieldMapping(this, fieldSetter);
    }

    public FieldMapping to(
            Consumer<LocalDate> fieldSetter
    ) {
        return asDateTo(date -> fieldSetter.accept(date.toLocalDate()));
    }

    public FieldMapping asPeriodTo(
            Consumer<Period> fieldSetter, Supplier<LocalDate> periodTo
    ) {
        return to(date -> fieldSetter.accept(
                Period.between(
                        date,
                        periodTo.get()
                )
        ));
    }

    public FieldMapping asPeriodToPresentTime(
            Consumer<Period> fieldSetter
    ) {
        return asPeriodTo(fieldSetter, LocalDate::now);
    }

    @Override public String type() {
        return "Date";
    }
}