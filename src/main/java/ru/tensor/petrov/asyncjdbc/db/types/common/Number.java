package ru.tensor.petrov.asyncjdbc.db.types.common;

import ru.tensor.petrov.asyncjdbc.db.types.DbField;
import ru.tensor.petrov.asyncjdbc.db.types.generic.FractionalField;
import ru.tensor.petrov.asyncjdbc.db.types.generic.IntegralField;

public class Number extends DbField implements IntegralField, FractionalField {

    public Number(String name) {
        super(name);
    }

    @Override public String type() {
        return "Number";
    }
}