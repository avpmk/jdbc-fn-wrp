package ru.tensor.petrov.asyncjdbc.db.types.common;

import ru.tensor.petrov.asyncjdbc.db.types.SizeableDbField;
import ru.tensor.petrov.asyncjdbc.db.types.generic.StringableField;

public class VarChar extends SizeableDbField implements
        StringableField {

    public VarChar(String name, int size) {
        super(name, size);
    }

    @Override public String type() {
        return "VarChar";
    }
}