package ru.tensor.petrov.asyncjdbc.db.types.filtering;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorNoIndex;

public interface Condition {

    void unrollTo(
            StringBuilder sql,
            List<PsInjectorNoIndex> injectorsToAdd
    );

    void unrollTo(
            AtomicInteger counter,
            StringBuilder sql,
            List<PsInjectorNoIndex> injectorsToAdd
    );
}