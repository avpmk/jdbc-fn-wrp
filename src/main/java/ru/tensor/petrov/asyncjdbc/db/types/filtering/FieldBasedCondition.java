package ru.tensor.petrov.asyncjdbc.db.types.filtering;

import ru.tensor.petrov.asyncjdbc.db.types.IDbField;

public abstract class FieldBasedCondition<I> implements Condition {

    protected final String fieldName;
    protected final I psInjector;

    public FieldBasedCondition(IDbField iDbField, I psInjector) {
        this.fieldName = iDbField.name();
        this.psInjector = psInjector;
    }

    protected abstract void makeSql(StringBuilder sql);

}