package ru.tensor.petrov.asyncjdbc.db.types.filtering;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import ru.tensor.petrov.asyncjdbc.db.types.IDbField;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorNoIndex;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorOneIndex;

public abstract class FieldBasedConditionOneParam extends FieldBasedCondition<PsInjectorOneIndex> {

    public FieldBasedConditionOneParam(IDbField iDbField, PsInjectorOneIndex psInjector) {
        super(iDbField, psInjector);
    }

    @Override public final void unrollTo(AtomicInteger counter, StringBuilder sql, List<PsInjectorNoIndex> injectorsToAdd) {
        int a = counter.incrementAndGet();
        injectorsToAdd.add(
                ps -> psInjector.inject(a, ps)
        );
        makeSql(sql);
    }

    @Override public final void unrollTo(StringBuilder sql, List<PsInjectorNoIndex> injectorsToAdd) {
        injectorsToAdd.add(
                ps -> psInjector.inject(1, ps)
        );
        makeSql(sql);
    }
}