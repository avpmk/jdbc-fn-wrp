package ru.tensor.petrov.asyncjdbc.db.types.filtering;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorNoIndex;

public abstract class WrapingCondition implements Condition {

    protected final Condition a, b;

    public WrapingCondition(Condition a, Condition b) {
        this.a = a;
        this.b = b;
    }

    @Override public final void unrollTo(StringBuilder sql, List<PsInjectorNoIndex> injectorsToAdd) {
        unrollTo(new AtomicInteger(), sql, injectorsToAdd);
    }

    @Override public abstract void unrollTo(AtomicInteger counter, StringBuilder sql, List<PsInjectorNoIndex> injectorsToAdd);
}