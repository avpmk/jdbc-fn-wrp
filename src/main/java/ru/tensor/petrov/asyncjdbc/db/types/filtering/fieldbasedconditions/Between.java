package ru.tensor.petrov.asyncjdbc.db.types.filtering.fieldbasedconditions;

import ru.tensor.petrov.asyncjdbc.db.types.IDbField;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.FieldBasedConditionTwoParam;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorTwoIndexs;

public class Between extends FieldBasedConditionTwoParam {

    public Between(IDbField iDbField, PsInjectorTwoIndexs psInjector) {
        super(iDbField, psInjector);
    }

    @Override protected void makeSql(StringBuilder sql) {
        sql.append(fieldName).append(" BETWEEN ? AND ?");
    }
}