package ru.tensor.petrov.asyncjdbc.db.types.filtering.fieldbasedconditions;

import ru.tensor.petrov.asyncjdbc.db.types.IDbField;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.FieldBasedConditionOneParam;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorOneIndex;

public class Like extends FieldBasedConditionOneParam {

    public Like(IDbField iDbField, PsInjectorOneIndex psInjector) {
        super(iDbField, psInjector);
    }

    @Override protected void makeSql(StringBuilder sql) {
        sql.append(fieldName).append(" LIKE ?");
    }
}