package ru.tensor.petrov.asyncjdbc.db.types.filtering.wrapingconditions;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.Condition;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.WrapingCondition;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorNoIndex;

public class Or extends WrapingCondition {

    public Or(Condition a, Condition b) {
        super(a, b);
    }

    @Override public void unrollTo(AtomicInteger counter, StringBuilder sql, List<PsInjectorNoIndex> injectorsToAdd) {
        sql.append("(");
        a.unrollTo(counter, sql, injectorsToAdd);
        sql.append(") OR (");
        b.unrollTo(counter, sql, injectorsToAdd);
        sql.append(")");
    }
}