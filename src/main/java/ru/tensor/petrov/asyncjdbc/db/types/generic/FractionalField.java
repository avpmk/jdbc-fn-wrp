package ru.tensor.petrov.asyncjdbc.db.types.generic;

import java.util.function.DoubleSupplier;
import ru.tensor.petrov.asyncjdbc.db.types.IDbField;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.Condition;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.fieldbasedconditions.Between;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.fieldbasedconditions.EqualTo;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.set.BiDoubleInjector;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.set.DoubleInjector;

import static ru.tensor.petrov.util.cortages.BiDouble.biDouble;

public interface FractionalField extends IDbField {

    @Deprecated default Condition isEqualTo(DoubleInjector fieldGetter) {
        return new EqualTo(this, fieldGetter);
    }

    default Condition isEqualTo(DoubleSupplier fieldGetter, double precision) {
        return isBetween(() -> {
            double value = fieldGetter.getAsDouble();
            return biDouble(value - precision, value + precision);
        });
    }

    default Condition isBetween(BiDoubleInjector fieldsGetter) {
        return new Between(this, fieldsGetter);
    }
}