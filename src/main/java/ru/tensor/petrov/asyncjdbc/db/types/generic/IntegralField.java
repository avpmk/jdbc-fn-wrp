package ru.tensor.petrov.asyncjdbc.db.types.generic;

import ru.tensor.petrov.asyncjdbc.db.types.FieldMapping;
import ru.tensor.petrov.asyncjdbc.db.types.IDbField;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.Condition;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.fieldbasedconditions.EqualTo;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.get.BigIntegerExtractor;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.get.IntExtractor;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.get.LongExtractor;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.set.IntInjector;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.set.LongInjector;

public interface IntegralField extends IDbField {

    //<editor-fold defaultstate="collapsed" desc="mappings (short names)">
    default FieldMapping to(
            IntExtractor extractor
    ) {
        return asIntTo(extractor);
    }

    default FieldMapping to(
            LongExtractor extractor
    ) {
        return asLongTo(extractor);
    }

    default FieldMapping to(
            BigIntegerExtractor extractor
    ) {
        return asBigIntTo(extractor);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="mappings">
    default FieldMapping asIntTo(
            IntExtractor extractor
    ) {
        return new FieldMapping(this, extractor);
    }

    default FieldMapping asLongTo(
            LongExtractor extractor
    ) {
        return new FieldMapping(this, extractor);
    }

    default FieldMapping asBigIntTo(
            BigIntegerExtractor extractor
    ) {
        return new FieldMapping(this, extractor);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="conditions">
    default Condition isEqualTo(IntInjector fieldGetter) {
        return new EqualTo(this, fieldGetter);
    }

    default Condition isEqualTo(LongInjector fieldGetter) {
        return new EqualTo(this, fieldGetter);
    }

//    default Condition isEqualTo(Supplier<BigInteger> fieldGetter) {
//        return new EqualTo(this, fieldGetter)
//    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="conditions with constants">
//    default Condition isEqualToConstant(int constant) {
//        return new Condition();
//    }
//    default Condition isEqualToConstant(long constant) {
//        return new Condition();
//    }
//    default Condition isEqualToConstant(BigInteger constant) {
//        return new Condition();
//    }
    //</editor-fold>

}