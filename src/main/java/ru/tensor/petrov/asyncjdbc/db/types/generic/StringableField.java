package ru.tensor.petrov.asyncjdbc.db.types.generic;

import java.util.function.Supplier;
import ru.tensor.petrov.asyncjdbc.db.types.FieldMapping;
import ru.tensor.petrov.asyncjdbc.db.types.IDbField;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.Condition;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.fieldbasedconditions.EqualTo;
import ru.tensor.petrov.asyncjdbc.db.types.filtering.fieldbasedconditions.Like;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.get.StringExtractor;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.set.StringInjector;

public interface StringableField extends IDbField {

    //<editor-fold defaultstate="collapsed" desc="mappings">
    default FieldMapping to(
            StringExtractor fieldSetter
    ) {
        return asStringTo(fieldSetter);
    }

    default FieldMapping asStringTo(
            StringExtractor fieldSetter
    ) {
        return new FieldMapping(this, fieldSetter);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="conditions">
    default Condition isEqualTo(StringInjector fieldGetter) {
        return new EqualTo(this, fieldGetter);
    }

    //<editor-fold defaultstate="collapsed" desc="like">
    default Condition like(StringInjector fieldGetter) {
        return new Like(this, fieldGetter);
    }

    default Condition startsLike(Supplier<String> fieldGetter) {
        return like(
                () -> fieldGetter.get() + "%"
        );
    }

    default Condition endsLike(Supplier<String> fieldGetter) {
        return like(
                () -> "%" + fieldGetter.get()
        );
    }
    //</editor-fold>
    //</editor-fold>

}