package ru.tensor.petrov.asyncjdbc.db.types.mapping;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface PsInjectorNoIndex {
    void inject(PreparedStatement ps) throws SQLException;
}