package ru.tensor.petrov.asyncjdbc.db.types.mapping;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface PsInjectorOneIndex {
    void inject(int index, PreparedStatement ps) throws SQLException;
}