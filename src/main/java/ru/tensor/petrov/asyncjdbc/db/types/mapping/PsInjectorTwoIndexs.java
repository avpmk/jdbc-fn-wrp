package ru.tensor.petrov.asyncjdbc.db.types.mapping;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface PsInjectorTwoIndexs {
    void inject(int indexA, int indexB, PreparedStatement ps) throws SQLException;
}