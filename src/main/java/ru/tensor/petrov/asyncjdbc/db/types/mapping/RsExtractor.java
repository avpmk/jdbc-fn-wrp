package ru.tensor.petrov.asyncjdbc.db.types.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RsExtractor {
    void extract(int index, ResultSet rs) throws SQLException;
}