package ru.tensor.petrov.asyncjdbc.db.types.mapping.get;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Consumer;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;

public interface BigIntegerExtractor extends RsExtractor, Consumer<BigInteger> {

    @Override default void extract(int index, ResultSet rs) throws SQLException {
        accept(
                rs.getBigDecimal(index).toBigInteger()
        );
    }
}