package ru.tensor.petrov.asyncjdbc.db.types.mapping.get;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Consumer;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;

public interface DateExtractor extends Consumer<Date>, RsExtractor {

    @Override public default void extract(int index, ResultSet rs) throws SQLException {
        accept(
                rs.getDate(index)
        );
    }
}