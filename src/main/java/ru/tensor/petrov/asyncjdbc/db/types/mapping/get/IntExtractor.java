package ru.tensor.petrov.asyncjdbc.db.types.mapping.get;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.IntConsumer;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;

public interface IntExtractor extends RsExtractor, IntConsumer {

    @Override default void extract(int index, ResultSet rs) throws SQLException {
        accept(
                rs.getInt(index)
        );
    }
}