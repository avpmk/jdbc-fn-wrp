package ru.tensor.petrov.asyncjdbc.db.types.mapping.get;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.LongConsumer;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;

public interface LongExtractor extends RsExtractor, LongConsumer {

    @Override public default void extract(int index, ResultSet rs) throws SQLException {
        accept(
                rs.getLong(index)
        );        
    }
}