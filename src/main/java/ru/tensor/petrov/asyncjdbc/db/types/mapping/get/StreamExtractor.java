package ru.tensor.petrov.asyncjdbc.db.types.mapping.get;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Consumer;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;

public interface StreamExtractor extends RsExtractor, Consumer<InputStream> {

    @Override default void extract(int index, ResultSet rs) throws SQLException {
        accept(
                rs.getBinaryStream(index)
        );
    }
}