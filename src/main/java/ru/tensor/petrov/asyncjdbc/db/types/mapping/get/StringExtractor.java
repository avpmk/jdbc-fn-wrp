package ru.tensor.petrov.asyncjdbc.db.types.mapping.get;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Consumer;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.RsExtractor;

public interface StringExtractor extends RsExtractor, Consumer<String> {

    @Override default void extract(int index, ResultSet rs) throws SQLException {
        accept(
                rs.getString(index)
        );
    }
}