package ru.tensor.petrov.asyncjdbc.db.types.mapping.set;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.function.Supplier;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorTwoIndexs;
import ru.tensor.petrov.util.cortages.BiDouble;

public interface BiDoubleInjector extends Supplier<BiDouble>, PsInjectorTwoIndexs {

    @Override default void inject(int indexA, int indexB, PreparedStatement ps) throws SQLException {
        BiDouble biDouble = get();
        ps.setDouble(indexA, biDouble.a);
        ps.setDouble(indexB, biDouble.b);
    }
}