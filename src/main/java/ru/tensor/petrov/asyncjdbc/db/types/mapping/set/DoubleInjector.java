package ru.tensor.petrov.asyncjdbc.db.types.mapping.set;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.function.DoubleSupplier;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorOneIndex;

public interface DoubleInjector extends DoubleSupplier, PsInjectorOneIndex {

    @Override default void inject(int index, PreparedStatement ps) throws SQLException {
        ps.setDouble(index, getAsDouble());
    }
}