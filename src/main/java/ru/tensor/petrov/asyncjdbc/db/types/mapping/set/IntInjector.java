package ru.tensor.petrov.asyncjdbc.db.types.mapping.set;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.function.IntSupplier;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorOneIndex;

public interface IntInjector extends IntSupplier, PsInjectorOneIndex {

    @Override default void inject(int index, PreparedStatement ps) throws SQLException {
        ps.setInt(index, getAsInt());
    }
}