package ru.tensor.petrov.asyncjdbc.db.types.mapping.set;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.function.LongSupplier;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorOneIndex;

public interface LongInjector extends LongSupplier, PsInjectorOneIndex {

    @Override default void inject(int index, PreparedStatement ps) throws SQLException {
        ps.setLong(index, getAsLong());
    }
}