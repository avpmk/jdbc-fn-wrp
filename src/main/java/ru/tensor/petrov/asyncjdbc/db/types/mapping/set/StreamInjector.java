package ru.tensor.petrov.asyncjdbc.db.types.mapping.set;

import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.function.Supplier;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorOneIndex;

public interface StreamInjector extends Supplier<InputStream>, PsInjectorOneIndex {

    @Override default void inject(int index, PreparedStatement ps) throws SQLException {
        ps.setBinaryStream(index, get());
    }
}