package ru.tensor.petrov.asyncjdbc.db.types.mapping.set;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.function.Supplier;
import ru.tensor.petrov.asyncjdbc.db.types.mapping.PsInjectorOneIndex;

public interface StringInjector extends Supplier<String>, PsInjectorOneIndex {

    @Override default void inject(int index, PreparedStatement ps) throws SQLException {
        ps.setString(index, get());
    }
}