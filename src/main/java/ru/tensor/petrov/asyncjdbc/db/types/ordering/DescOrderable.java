package ru.tensor.petrov.asyncjdbc.db.types.ordering;

public final class DescOrderable implements Orderable {

    final Orderable parent;

    public DescOrderable(Orderable parent) {
        this.parent = parent;
    }

    @Override public String name() {
        return parent.name() + " DESC";
    }
}