package ru.tensor.petrov.asyncjdbc.db.types.ordering;

public interface Orderable {

    String name();

    default Orderable desc() {
        if (this instanceof DescOrderable) {
            return ((DescOrderable) this).parent;
        } else {
            return new DescOrderable(this);
        }
    }
}