package ru.tensor.petrov.asyncjdbc.util;

import java.math.BigInteger;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.function.LongConsumer;

public class ConsumerUtil {

    public static IntConsumer intAsString(Consumer<String> clientConsumer) {
        return digit -> clientConsumer.accept(
                Integer.toString(digit)
        );
    }

    public static LongConsumer longAsString(Consumer<String> clientConsumer) {
        return digit -> clientConsumer.accept(
                Long.toString(digit)
        );
    }

    public static Consumer<BigInteger> bigIntAsString(Consumer<String> clientConsumer) {
        return digit -> clientConsumer.accept(
                digit.toString()
        );
    }
}