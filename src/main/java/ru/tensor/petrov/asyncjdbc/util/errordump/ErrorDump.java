package ru.tensor.petrov.asyncjdbc.util.errordump;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import ru.tensor.petrov.asyncjdbc.db.DbProvider;

import static java.lang.System.out;
import static ru.tensor.petrov.util.jarpath.JarPath.jarDir;

public class ErrorDump {

    public static void dumpErrorToFile(String filePrefix, String msg) {
        String fileName = filePrefix + System.currentTimeMillis() + ".txt";
        Path pathWriteTo = jarDir(DbProvider.class).getParent().resolve(fileName);
        try (BufferedWriter writer = Files.newBufferedWriter(
                pathWriteTo,
                StandardCharsets.UTF_8
        )) {
            out.println(msg);
            writer.write(msg);
        } catch (IOException ioe) {
            out.println(
                    "Ошибка при создании файла " + fileName + " : " + ioe
            );
        }
    }
}