package ru.tensor.petrov.asyncjdbc.util.fi;

import java.sql.SQLException;

public interface SupplierSqle<T> {
    T get() throws SQLException;
}